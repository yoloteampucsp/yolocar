# YoloCar

Sistema para locação de carros
Trabalho referente a matéria Engenharia de Software e Modelagem - PUCSP 2019

## BAIXANDO O PROJETO

Para baixar o projeto é necessario ter o GIT instalado no computador.
 
Baixe o git neste link https://git-scm.com/download/win e instale com as configurações padrões, 
alterando apenas o terminal padrão.

Configure uma sshkey conforme o tutorial abaixo
https://www.princiweb.com.br/blog/programacao/controle-de-versao/como-usar-ssh-key-no-git.html

Após isso entre no diretorio onde você deseja clonar o projeto (ou indique um) e rode o comando através do prompt de comando do Windows:
git clone git@gitlab.com:yoloteampucsp/yolocar.git

Pronto, o projeto será clonado no diretorio indicado :)