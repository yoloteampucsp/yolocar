package br.pucsp.yolocar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class YolocarApplication

fun main(args: Array<String>) {
	runApplication<YolocarApplication>(*args)
}
